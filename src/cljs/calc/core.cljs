(ns calc.core
  (:refer-clojure :exclude [number?])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-tools.core :refer-macros [defcomponentk]]))

(enable-console-print!)

;;; Forward Declarations

(def ^:private buttons
  [["RAD" "DEG" "x!" "(" ")" "%" "AC"]
   ["Inv" "sin" "ln" 7 8 9 "÷"]
   ["π" "cos" "log" 4 5 6 "×"]
   ["e" "tan" "√" 1 2 3 "-"]
   ["Ans" "EXP" "x^y" 0 "." "=" "+"]])

(declare on-input build-row)

;;; Public

(defcomponentk screen
  [[:data input] owner opts]
  (render
   [_]
   (ReactBootstrap/Well #js {:className "calc-screen"}
                        (if (empty? (:stack input))
                          "0"
                          (clojure.string/join (:stack input))))))

(defcomponentk btn
  [[:data input title {width 98} {height 50}]
   [:opts class-name]]
  (render
   [_]
   (ReactBootstrap/Button #js {:style #js {:width (str width "px")
                                           :height (str height "px")}
                               :onClick #(on-input title input)
                               :className class-name}
                          title)))

(defcomponentk button-grid
  [[:data input] owner opts]
  (render
   [_]
   (dom/div #js {:className "numpad-container"}
            (apply ReactBootstrap/Grid
                   nil
                   (map #(apply build-row input (nth buttons %))
                        (take 5 (range)))))))

(defcomponentk calculator
  [[:data input] owner opts]
  (render
   [_]
   (dom/div #js {:className "calc-container centered"}
            (om/build screen {:input input})
            (om/build button-grid {:input input}))))

;;; Implementation

(defn- factorial
  [x]
  (loop [n x
         f 1]
    (if (= n 1)
      f
      (recur (dec n) (* f n)))))

(defn- number?
  [n]
  (when-not (js/isNaN n) n))

(defn- op-lookup
  [x]
  (get {"x!" factorial
        "%" #(* % 0.01)
        "Inv" #(if-not (zero? %) (/ 1 %) %)
        "sin" #(.sin js/Math %)
        "cos" #(.cos js/Math %)
        "tan" #(.tan js/Math %)
        "ln" #(.log js/Math %)
        "log" #(.log js/Math %)
        "+" #(partial + %)
        "-" #(partial - %)
        "×" #(partial * %)
        "÷" #(partial / %)
        "." #(comp js/parseFloat (partial str % "."))} x))

(defn- reduce-stack
  [stack]
  [(reduce (fn [result val]
             (if (fn? result)
               (result val)
               ((op-lookup val) result)))
           stack)])

;; 1. fractions not working
;; 2. logarithm does not take two numbers yet

(defn- on-input
  [x state]
  (let [stack (:stack @state)
        top (last stack)]
    (condp = x
      "=" (om/transact! state :stack reduce-stack)
      "AC" (om/update! state :stack [0])
      "π" (om/transact! state :stack #(conj % (.-PI js/Math)))
      "e" (om/transact! state :stack #(conj % (.-E js/Math)))
      (if (and (number? top) (number? x))
        (om/transact! state :stack #(assoc % (dec (count %)) (js/parseFloat (str (last %) x))))
        (when-not (and (op-lookup x) (op-lookup top))
          (if (and (= 1 (count stack))
                   (number? ((op-lookup x) top))
                   (not= x "."))
            (om/transact! state :stack #(reduce-stack (conj stack x)))
            (om/transact! state :stack #(conj % x))))))))

(defn- build-row
  [input-cursor & items]
  (apply ReactBootstrap/Row #js {:className "numpad-row"}
         (map (fn [item]
                (ReactBootstrap/Col
                 #js {:xs 1}
                 (om/build btn
                           {:title (or (:title item) item)
                            :input input-cursor}
                           {:opts {:class-name (:class-name item)}})))
              items)))

;;; Root

(def app-state (atom {:input {:stack []}}))

(om/root calculator
         app-state
         {:target (. js/document (getElementById "my-app"))
          :tx-listen (fn [m _] (do (println (str "OLD STATE: " (:old-state m)))
                                  (println (str "NEW STATE: " (:new-state m)))))})
