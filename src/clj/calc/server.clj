(ns calc.server
  (require [immutant.web :as web]
           [liberator.core :refer [resource defresource]]
           [ring.middleware.params :refer [wrap-params]]
           [compojure.core :refer [defroutes GET]]
           [compojure.route :as route]
           [compojure.handler :as handler]
           [clojure.java.io :as io]))

(defresource site
  []
  :allowed-methods [:get :options]
  :available-media-types ["text/html"]
  :handle-ok (fn [ctx] (slurp (io/resource "public/index.html"))))

(defroutes app
  (route/resources "/")
  (GET "/" [] (site)))

(defn- wrap-nil-body
  [handler]
  (fn [request]
    (let [response (handler request)]
      (update-in response [:body] #(or % "")))))

(def handler
  (-> app
      wrap-params
      handler/api
      wrap-nil-body))

(defn start
  [& [port]] (web/run (fn [request] (handler request)) {:host "localhost" :port (or port 8080)}))
