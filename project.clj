(defproject calc "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2311"]
                 [org.immutant/web "2.x.incremental.247"]
                 [prismatic/om-tools "0.3.2"]
                 [ring/ring-core "1.3.0"]
                 [compojure "1.1.8"]
                 [liberator "0.12.0"]
                 [om "0.7.1"]]
  :resource-paths ["resources"]
  :plugins [[lein-cljsbuild "1.0.3"]]
  :source-paths ["src/clj"]
  :repositories [["Immutant incremental builds"
                  "http://downloads.immutant.org/incremental/"]]
  :cljsbuild {:builds {:main {:source-paths ["src/cljs"]
                              :compiler {:output-to "resources/public/js/main.js"
                                         :output-dir "out"
                                         :optimizations :whitespace
                                         :pretty-print false
                                         :source-ap "target/cljsbuild/public/js/main.js.map"}}}})
